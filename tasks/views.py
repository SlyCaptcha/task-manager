from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from .models import Task
from .serializers import TaskModelSerializer

# Create your views here.

class TaskModelViewSet(ModelViewSet):
    """
    Возвращает список всех запланированных задач, а также каждую по
    отдельности по id.
    """
    serializer_class = TaskModelSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, )
    filterset_fields = ('planned_completion_date', 'status', )

    def get_queryset(self):
        """
        Возвращает объект QuerySet запланированных задач с возможностью
        фильтарции по статусу и планируемому времени выполнения через URL.
        """
        username = self.request.user
        return Task.objects.filter(author=username, is_displayed=True)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class TasksHistoryListAPIView(ListAPIView):
    """
    Возвращет полную историю изменений созданной задачи.
    """
    queryset = Task.history.all()
    serializer_class = TaskModelSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, )

    # def get_queryset(self):
    #     queryset = Task.history.all()
    #     return queryset
