from datetime import timedelta

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import include, path, reverse
from django.utils.timezone import now
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APITestCase

from .models import Task
from .views import TaskModelViewSet

DEFAUL_USER_MODEL = get_user_model()
CURRENT_DATE = now().date()
FUTURE_DATE = CURRENT_DATE + timedelta(days=7)

# Create your tests here.

class TaskTestCase(TestCase):
    def setUp(self):
        test_user = DEFAUL_USER_MODEL.objects.create_user(
            username='test_user',
            email='test_user@site.com',
            password='test_password'
        )
        another_test_user = DEFAUL_USER_MODEL.objects.create_user(
            username='another_test_user',
            email='another_test_user@site.com',
            password='another_test_password'
        )

        Task.objects.create(
            title='Test task',
            description='Test tasks description.',
            author=test_user,
            date_created=CURRENT_DATE
        )
        Task.objects.create(
            title='Another test task',
            description='Another test tasks description.',
            author=another_test_user,
            date_created=CURRENT_DATE,
            status='IS_PLANNED'
        )

    def test_create_task(self):
        """
        Проверка создания задачи через менеджер объекта.
        """
        test_user = DEFAUL_USER_MODEL.objects.get(username='test_user')
        test_task = Task.objects.get(title='Test task')

        self.assertEqual(
            test_task.description,
            'Test tasks description.'
        )
        self.assertEqual(test_task.author, test_user)
        self.assertEqual(test_task.date_created, CURRENT_DATE)
        self.assertEqual(test_task.planned_completion_date, FUTURE_DATE)
        self.assertEqual(test_task.date_of_actual_completion, FUTURE_DATE)
        self.assertEqual(test_task.status, 'CREATED')
        self.assertTrue(test_task.is_displayed)


    def test_update_task(self):
        """
        Проверка изменения задачи через менеджер объекта.
        """
        another_test_user = DEFAUL_USER_MODEL.objects.get(
            username='another_test_user'
        )
        another_test_task = Task.objects.get(
            title='Another test task'
        )
        updated_task_description = 'Another test tasks description. Checked!'
        status_complete = 'COMPLETED'

        self.assertEqual(
            another_test_task.description,
            'Another test tasks description.'
        )

        # Обновление содержания поля description
        another_test_task.description = updated_task_description
        # Изменение статуса на COMPLETED и вызов метода изменения поля
        # date_of_actual_completion (дата фактического выполнения задачи) на
        # текущую дату.
        another_test_task.status = status_complete
        another_test_task.complete_task()

        self.assertEqual(
            another_test_task.description,
            updated_task_description
        )
        self.assertEqual(another_test_task.status, status_complete)
        self.assertEqual(
            another_test_task.date_of_actual_completion,
            CURRENT_DATE
        )


class TaskAPITestCase(APITestCase):
    def setUp(self):
        self.test_user = self.create_test_user()
        self.test_user_token = self.create_test_users_token(self.test_user)
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f'Token {self.test_user_token.key}'
        )
        self.uri = '/tasks/'

    @staticmethod
    def create_test_user():
        return DEFAUL_USER_MODEL.objects.create_user(
            username='test_user',
            email='test_user@site.com',
            password='test_password'
        )

    @staticmethod
    def create_test_users_token(new_user):
        token = Token.objects.create(user=new_user)
        token.save()
        return token

    def test_api_task_list(self):
        """
        Проверка получения списка задач через API.
        """
        self.client.login(username='test_user', password='test_password')
        response = self.client.get(self.uri)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_create_task(self):
        """
        Проверка создания задачи через API.
        """
        self.client.login(
            username=self.test_user.username,
            password=self.test_user.password
        )
        test_data = {
            'title': 'Test task title',
            # 'author': self.test_user.id,
        }
        response = self.client.post(self.uri, test_data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Task.objects.count(), 1)
        self.assertEqual(Task.objects.get().author, self.test_user)

    def test_api_get_task_by_id(self):
        """
        Проверка получения задачи по id через API.
        """
        self.client.login(
            username=self.test_user.username,
            password=self.test_user.password
        )
        test_data = {
            'title':'Test task',
            'description':'Test tasks description.',
            # 'author':self.test_user,
            # 'date_created':CURRENT_DATE,
        }
        self.client.post(self.uri, test_data)
        test_task = Task.objects.get()
        response = self.client.get(f'{self.uri}{test_task.id}/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data['description'], 'Test tasks description.'
        )

    def test_api_update_task(self):
        """
        Проверка изменения задачи через API.
        """
        self.client.login(
            username=self.test_user.username,
            password=self.test_user.password
        )
        test_data = {
            'title': 'Test task',
            'description': 'Test tasks description.',
        }
        self.client.post(self.uri, test_data)
        test_task = Task.objects.get()
        test_task_url = f'{self.uri}{test_task.id}/'
        request = self.client.get(test_task_url)
        request.data['description'] = 'Test tasks description.CHECKED!'
        response = self.client.put(test_task_url, request.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data['description'],
            request.data['description']
        )
