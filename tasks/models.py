from datetime import date, timedelta
from uuid import uuid4

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db.models import (CASCADE, BooleanField, CharField, DateField,
                              ForeignKey, Model, TextField, UUIDField)
from simple_history.models import HistoricalRecords

CURRENT_DATE = date.today

# Create your models here.


def _set_future_date():
    return date.today() + timedelta(days=7)


class Task(Model):
    """
    Модель задач.
    """

    TASK_STATUSES = [
        ('CREATED', 'Created'),
        ('IS_PLANNED', 'Is planned'),
        ('IN_PROGRESS', 'In progress'),
        ('COMPLETED', 'Сompleted'),
    ]

    id = UUIDField(default=uuid4, primary_key=True, editable=False)
    title = CharField(max_length=255)
    description = TextField(blank=True)
    author = ForeignKey(
        get_user_model(),
        on_delete=CASCADE,
        related_name='tasks',
        blank=True,
    )
    date_created = DateField(default=CURRENT_DATE)
    planned_completion_date = DateField(
        default=_set_future_date,
    )
    date_of_actual_completion = DateField(
        default=_set_future_date,
    )
    status = CharField(
        max_length=255,
        choices=TASK_STATUSES,
        default='CREATED'
    )
    date_updated = DateField(auto_now=True)
    is_displayed = BooleanField(default=True)
    history = HistoricalRecords()

    class Meta:
        db_table = 'tasks'

    def __str__(self):
        return self.title

    def clean(self):
        """
        Проверка полей планируемой даты и фактической даты завершения задачи,
        что они не раньше, чем дата создания самой задачи.
        """
        if self.planned_completion_date < self.date_created:
            raise ValidationError(
                'The \"Planned completion date\" cannot be earlier than the creation date!'
            )
        if self.date_of_actual_completion < self.date_created:
            raise ValidationError(
                'The \"Date of actual completion\" cannot be earlier than the creation date!'
            )

    def complete_task(self):
        """
        Возвращает дату фактического завершения задачи при изменении статуса
        на 'COMPLETED'.
        """
        if self.status == 'COMPLETED':
            self.date_of_actual_completion = CURRENT_DATE()
            return self.date_of_actual_completion

    def save(self, *args, **kwargs):
        self.complete_task()
        super(Task, self).save(*args, **kwargs)
