from django.contrib.admin import ModelAdmin, register

from .models import Task

# Register your models here.

@register(Task)
class TaskModelAdmin(ModelAdmin):
    list_display = (
        'title',
        'author',
        'date_created',
        'planned_completion_date',
        'status',
        'is_displayed'
    )
    list_filter = (
        'date_created',
        'status',
        'is_displayed'
    )
    search_fields = (
        'title',
        'author',
        'status',
    )
    ordering = (
        '-title',
        '-date_created',
    )

    def save_model(self, request, obj, form, change):
        # todo: fix keeping only original author
        obj.author = request.user
        super().save_model(request, obj, form, change)
