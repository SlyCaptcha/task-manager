from rest_framework.serializers import ModelSerializer

from .models import Task


class TaskModelSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = (
            'id',
            'title',
            'description',
            'author',
            'date_created',
            'planned_completion_date',
            'date_of_actual_completion',
            'status',
            'date_updated',
        )
