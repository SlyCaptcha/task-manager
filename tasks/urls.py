from django.urls import path
from rest_framework.routers import SimpleRouter

from .views import TaskModelViewSet, TasksHistoryListAPIView

app_name = 'tasks'

router = SimpleRouter()
router.register('tasks', TaskModelViewSet, basename='tasks')

urlpatterns = [
    path('tasks/<uuid:pk>/history/', TasksHistoryListAPIView.as_view(), name='tasks-history'),
]
urlpatterns += router.urls
