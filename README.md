# TASK MANAGER

## Требования реализации
### Технологические требования:
- [X] Сервис должен быть написан на python3 с использованием любого веб-фреймворка (Django, flask, aiohttp…)
- [X] Сервис должен хранить данные в реляционной базе данных (postgresql, mysql)
- [X] Сервис должен предоставлять интерфейс в виде JSON API, это единственный способ общения клиента с сервисом
- [X] Авторизация в API происходит с помощью токена (переданного в заголовке Authorization)
- [X] Плюсом будет, если сервис будет покрыт тестами
- [X] Плюсом будет, если сервис будет содержать Dockerfile для сборки сервиса
- [X] Плюсом будет подробный README с описанием реализованного API и пояснениями как именно реализуются нижеприведённые функциональные требования
- [X] Делать GUI/frontend не требуется, нужен только сам сервис
### Функциональные требования:
- [X] Пользователь может зарегистрироваться в сервисе задав пару логин-пароль
- [X] В системе может существовать много пользователей
- [X] Пользователь может авторизоваться в сервисе предоставив пару логин-пароль и получив в ответе токен
- [X] Пользователь видит только свои задачи
- [X] Пользователь может создать себе задачу. Задача должна как минимум содержать следующие данные (*) обязательные поля):
    - *Название
    - *Описание
    - *Время создания
    - *Статус - один из Новая, Запланированная, в Работе, Завершённая
    - Планируемое дата завершения
- [X] Пользователь может менять статус задачи на любой из данного набора
- [X] Пользователь может менять планируемое время завершения, название и описание
- [X] Пользователь может получить список задач своих задач, с возможностью фильтрации по статусу и планируемому времени завершения
- [X] Большим плюсом будет возможность просмотреть историю изменений задачи (названия, описания, статуса, времени завершения)

## Создано с помощью
Django, Django REST, PostgreSQL.

## Установка
Для установки сервиса необходим установленный Docker.
```
docker build .
docker-compose up -d --build
```

## Реализация работы
*Команды приведены на примере утилиты "curl".*

### Регистрация и аутентификация в системе
Для регистрации в системе пользователь должен передать в запросе логин и пароль:
```
curl -X POST <site.com>/auth/registration/ -d "username=<username>&password1=<password>&password2=<password>"
```
Для авторизации в системе пользователь должен передать в запросе логин и пароль:
```
curl -X POST <site.com>/auth/login/ -d "username=<username>&password=<password>"
```

В ответ сервис отправит токен. Токен необходимо будет добавлять в каждый запрос в дальнейшем.
```json
{"key":"<token>"}
```

Сервис предоставляет ответы в формате `JSON`.

### Работа с планированием задач
#### Описание модели задачи
- id - автоикрементируемое uuid значение;
- title - заколовок записи;
- description - описание;
- author - автор записи;
- date_created - дата создания, по умолчанию - текущая дата;
- planned_completion_date - планируемая дата выполнения;
- date_of_actual_completion - фактическая дата выполнения;
- status - состоит из следущих вариантов:
    - Новая ('CREATED');
    - Запланированная ('IS_PLANNED');
    - В работе ('IN_PROGRESS');
    - Завершённая ('COMPLETED').
- date_updated - дата последненго обновления записи.

#### Добавление новой задачи:
Обязательные поля для заполнения:
- title
Необязательные поля:
- description
Поля со значениями по умолчанию:
- author
- date_created (значение по умолчанию: текущая дата)
- planned_completion_date (значение по умолчанию: текущая дата + 7 календарных дней)
- date_of_actual_completion (значение по умолчанию: текущая дата + 7 календарных дней; изменяется вручную или автоматически на текущую даты при изменении статуса на 'COMPLETED')
- status (значение по умолчанию: 'CREATED')
- date_updated - поле обновляется автоматически, недоступно для редактирования.
```
curl -X POST localhost:8000/tasks/ -H "Authorization: Token <token>" -d "title=<title>"
```
Пример ответа сервиса добавления записи:
```json
{"id":"75d13231-548e-47b6-9632-e00827d15b9f","description":"Tasks description.","author":2,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"CREATED","date_updated":"2020-10-06"}
```

#### Получение списка всех задач:
```
curl -X GET localhost:8000/tasks/ -H "Authorization: Token <token>"
```
Пример запроса всех задач пользователя:
```
curl -X GET localhost:8000/tasks/ -H "Authorization: Token f54634ce1a438df346440c3797c89552659a05c4"
```
```json
[{"id":"f7350aea-c9fc-4e8b-b6ea-7a843a1b29a7","title":"New_taks","description":"Tasks description. UPDATED!","author":2,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"CREATED","date_updated":"2020-10-06"},{"id":"9d278d7c-1226-4075-9dc0-4cadcd98f2ee","title":"Second task","description":"Second tasks description.","author":2,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"IS_PLANNED","date_updated":"2020-10-06"}]
```

#### Фильтрация списка задач:
Список задач можно отфильтровать по следующим полям:
- status
- planned_completion_date
```
curl -X GET localhost:8000/tasks/?<some_field>=<some_value> -H "Authorization: Token <token>"
```
Пример запроса задач пользователя c фильтром:
```
curl -X GET localhost:8000/tasks/?planned_completion_date=2020-10-13&status=IS_PLANNED-H "Authorization: Token f54634ce1a438df346440c3797c89552659a05c4"
```
```json
[{"id":"9d278d7c-1226-4075-9dc0-4cadcd98f2ee","title":"Second task","description":"Second tasks description.","author":2,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"IS_PLANNED","date_updated":"2020-10-06"}]
```

#### Получение записи по идентификатору:
```
curl -X GET localhost:8000/tasks/<id>/ -H "Authorization: Token <token>"
```
Пример запроса записи по идентификатору:
```
curl -X GET localhost:8000/tasks/9d278d7c-1226-4075-9dc0-4cadcd98f2ee/ -H "Authorization: Token f54634ce1a438df346440c3797c89552659a05c4"
```
```json
{"id":"9d278d7c-1226-4075-9dc0-4cadcd98f2ee","title":"Second task","description":"Second tasks description.","author":2,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"IS_PLANNED","date_updated":"2020-10-06"}
```

#### Изменение записи:
Пользователю доступны следующие поля для измнения:
- title
- description
- date_created
- planned_completion_date
- date_of_actual_completion
- status
```
curl -X PUT localhost:8000/tasks/<id>/ -H "Authorization: Token <token>" -d "{<some_field>=<some_value> ... &<some_field_n>=<some_value_n>}"
```
Пример запроса на изменение записи:
```
curl -X PUT localhost:8000/tasks/75d13231-548e-47b6-9632-e00827d15b9f/ -H "Authorization: Token f54634ce1a438df346440c3797c89552659a05c4" -d "title=New taks&description=Tasks description. UPDATED!"
```
```json
{"id":"75d13231-548e-47b6-9632-e00827d15b9f","description":"Tasks description. UPDATED!","author":2,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"CREATED","date_updated":"2020-10-06"}
```

#### Получение истории изменений записи по идентификатору:
```
curl -X GET localhost:8000/tasks/<id>/histoty/ -H "Authorization: Token <token>"
```
Пример запроса истории изменений записи по идентификатору:
```
curl -X GET localhost:8000/tasks/9d278d7c-1226-4075-9dc0-4cadcd98f2ee/history/ -H "Authorization: Token f54634ce1a438df346440c3797c89552659a05c4"
```
```json
[{"id":"eea3f139-7a9b-4a57-a452-1881c1b58be0","title":"Task","description":"Tasks description.","author":1,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"CREATED","date_updated":"2020-10-06"},
{"id":"eea3f139-7a9b-4a57-a452-1881c1b58be0","title":"Task","description":"Tasks description.","author":1,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"CREATED","date_updated":"2020-10-06"},
{"id":"eea3f139-7a9b-4a57-a452-1881c1b58be0","title":"Task","description":"Tasks description.","author":1,"date_created":"2020-10-06","planned_completion_date":"2020-10-13","date_of_actual_completion":"2020-10-13","status":"CREATED","date_updated":"2020-10-06"}]
```

#### Удаление записи:
```
curl -X DELETE localhost:8000/tasks/<id>/ -H "Authorization: Token <token>"
```
В ответ сервис вернет пустую строку.
